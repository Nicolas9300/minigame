package juego;

import java.awt.*;

import entorno.Entorno;
import entorno.Herramientas;

public class Estrella {

	private double x;
	private double y;
	private double velocidad;
	private Image estrella;

	public Estrella(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 7;
		this.estrella = Herramientas.cargarImagen("Estrella.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(estrella, x, y, 0, 1);
	}

	public void mover() {
		x -= velocidad;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double ancho() {
		return estrella.getWidth(null);
	}

	public double getAlto() {
		return estrella.getHeight(null);
	}

	public void respawnearEstrella(Entorno entorno) {
		x = 7000;
	}

	public boolean sePaso() {
		return x < 0;

	}
}
