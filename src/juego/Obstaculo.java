package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {

	private double x;
	private double y;
	private double velocidad;
	private Image pinchos;

	public Obstaculo(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 3;
		this.pinchos = Herramientas.cargarImagen("Pinchos.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(pinchos, x, y, 0, 1);
	}

	public void mover() {
		x -= velocidad;
	}

	public boolean sePaso() {
		return x < 0;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int anchoDelPincho() {
		return pinchos.getWidth(null);
	}

	public int altoDelPincho() {
		return pinchos.getHeight(null);
	}

	public void desaparecer() {
		y = 5000;
	}
}
