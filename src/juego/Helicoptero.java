package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Helicoptero {

	private double x;
	private double y;
	private double velocidad;
	private Image helicoptero;

	public Helicoptero(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 2;
		this.helicoptero = Herramientas.cargarImagen("Helicoptero.gif");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(helicoptero, x, y, 0, 1);
	}

	public void mover() {
		x -= velocidad;
		y += 3 * Math.sin(x / 50);
	}

	public boolean sePaso() {
		return x == -100;
	}

	// la convención en Java es ponerle nombres que empiezan con minúscula a los métodos
	public int anchoDelHelicoptero() {
		return helicoptero.getWidth(null);
	}

	// la convención en Java es ponerle nombres que empiezan con minúscula a los métodos
	public int altoDelHelicoptero() {
		return helicoptero.getHeight(null);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void desaperecerHelicoptero() {
		x -= 5000;
	}

	public boolean desaparecio() {
		return x < -200;
	}

}
