package juego;

import java.awt.*;
import java.util.LinkedList;

import entorno.*;

public class Princesa {

	private int vidas;
	private double x;
	private double y;
	private double velocidad;
	private double velocidadSalto;
	private double gravedad;
	private double moverConRebote;
	private Image princesa; 

	public Princesa(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 4;
		this.velocidadSalto = 8;
		this.gravedad = 0.1 + 8 - velocidadSalto;
		this.moverConRebote = 5;
		this.princesa = Herramientas.cargarImagen("Princesa.png");
		this.vidas = 3;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(princesa, x, y, 0, 1);
	}
	public int Total_Vidas() {
		return vidas;
	}

	public void restarVida() {
		vidas -= 1;
	}

	public void moverIzquierda() {
		x -= velocidad;
	}

	public void moverDerecha() {
		x += velocidad;
	}

	public void saltar() {
		y -= velocidadSalto;
		if (velocidadSalto > 0) {
			velocidadSalto -= 0.1;
		}
	}

	public void caer() {
		if (y > 500) { 
			y = 500;
		}
		if (y < 500) {
			y += gravedad;
			gravedad = gravedad + 0.1;
		}
		if (y == 500) {
			velocidadSalto = 8;
			gravedad = 0.1 + 8 - velocidadSalto;
		}
	}

	public boolean hayLimiteIzquierda() {
		return x < 40;
	}

	public boolean hayLimiteDerecha(Entorno entorno) {
		return x > entorno.ancho() / 2;
	}

	public void moverDerechaConRebote() {
		x += moverConRebote;
	}

	public void moverIzquierdaConRebote() {
		x -= moverConRebote;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int anchoDeLaPrincesa() {
		return princesa.getWidth(null);
	}

	public int altoDeLaPrincesa() {
		return princesa.getHeight(null);
	}

	public int getVidas() {
		return vidas;
	}

	public Fuego dispararFuego() {
		return new Fuego(x, y);
	}

	public Image mirarHaciaAtras() {
		return this.princesa = Herramientas.cargarImagen("PrincesaAtras.png");
	}

	public Image imagenDisparando() {
		return this.princesa = Herramientas.cargarImagen("PrincesaAtaca.png");
	}

	public Image mirarHaciaAdelante() {
		return this.princesa = Herramientas.cargarImagen("Princesa.png");
	}

	public boolean chocasteConPinchos(LinkedList<Obstaculo> obstaculos) {
		return x + princesa.getWidth(null) / 2 >= obstaculos.getFirst().getX()
				- (obstaculos.getFirst().anchoDelPincho() / 2 - 10)
				&& x - princesa.getWidth(null) / 2 <= obstaculos.getFirst().getX()
						+ obstaculos.getFirst().anchoDelPincho() / 2
				&& y + princesa.getHeight(null) / 2 >= obstaculos.getFirst().getY()
						- (obstaculos.getFirst().altoDelPincho() / 2 + 20);
	}

	public boolean chocasteConHelicoptero(Helicoptero h) {
		if (y - (princesa.getHeight(null) / 2) < h.getY() + (h.altoDelHelicoptero() / 2)) {
			if (h.getX() - (h.anchoDelHelicoptero() / 2) > x - (princesa.getWidth(null) / 2)) {
				if (h.getX() - (h.anchoDelHelicoptero() / 2) < x + (princesa.getWidth(null) / 2)) {
					return true;
				}
			}

			if (h.getX() + (h.anchoDelHelicoptero() / 2) > x - (princesa.getWidth(null) / 2)) {
				if (h.getX() + (h.anchoDelHelicoptero() / 2) < x + (princesa.getWidth(null) / 2)) {
					return true;
				}
			}
			if (h.getX() - (h.anchoDelHelicoptero() / 2) < x - (princesa.getWidth(null) / 2)) {
				if (h.getX() + (h.anchoDelHelicoptero() / 2) > x + (princesa.getWidth(null) / 2)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean chocasteConSoldado(LinkedList<Soldado> s) {
		return x + anchoDeLaPrincesa() / 2 >= s.getFirst().getX() - s.getFirst().anchoDelSoldado() / 2
				&& x - anchoDeLaPrincesa() / 2 <= s.getFirst().getX() + s.getFirst().anchoDelSoldado() / 2
				&& y + altoDeLaPrincesa() / 2 >= s.getFirst().getY() - s.getFirst().altoDelSolado() / 2;
	}

	public boolean agarrasteEstrella(Estrella e) {
		return x + anchoDeLaPrincesa() / 2 >= e.getX() - e.ancho() / 2 && x - anchoDeLaPrincesa() / 2 <= e.getX() + e.ancho()
				&& y - altoDeLaPrincesa() / 2 <= e.getY() + e.getAlto();
	}

}
