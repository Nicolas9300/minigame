package juego;

import java.awt.*;
import java.util.*;

import javax.sound.sampled.Clip;

import entorno.*;

public class Juego extends InterfaceJuego {


	private Entorno entorno;

	private Suelo suelo;
	private Image fondo;
	private Image gameover;
	private Image corazon; 

	private Integer puntuacion;
	private Integer puntuacionParaBoss;
	private int timerInmortal;
	private int timer;
	private int NumeroRandom;
	
	

	private Princesa princesa;
	private Helicoptero helicoptero;
	private Estrella estrella;
	private LinkedList<Soldado> soldados;
	private LinkedList<Obstaculo> obstaculos;
	private LinkedList<Fuego> fuegos;

	public Juego() {
		this.entorno = new Entorno(this, "El Juego: Super Elizabeth Sis", 800, 600);
		
		//Random
		Random r = new Random();
		int NumeroMenor= 300;
		int NumeroMayor = 400;
	    NumeroRandom = r.nextInt(NumeroMayor-NumeroMenor) + NumeroMenor;

		// Timer
		timer = 0;

		// Puntuaciones y vidas
		puntuacion = 0;
		puntuacionParaBoss = 150;
		

		// Background
		suelo = new Suelo(400, entorno.alto() - 36);
		fondo = Herramientas.cargarImagen("Fondo.jpg");
		gameover = Herramientas.cargarImagen("Gameover.jpg");
		corazon = Herramientas.cargarImagen("Corazon.gif");

		// Princesa
		princesa = new Princesa(entorno.ancho() / 4, entorno.alto() - 100);
		timerInmortal = 200;

		// Helicoptero
		helicoptero = new Helicoptero(entorno.ancho() + 200, entorno.alto() / 2 + 100);

		// Estrella
		estrella = new Estrella(entorno.ancho() + 5000, entorno.alto() / 2 + 40);

		// Soldados
		soldados = new LinkedList<Soldado>();
		soldados.add(new Soldado(entorno.ancho() + 300, entorno.alto() - 115));
		soldados.add(new Soldado(entorno.ancho() + 600, entorno.alto() - 115));
		soldados.add(new Soldado(entorno.ancho() + 900, entorno.alto() - 115));

		// Obstaculos
		obstaculos = new LinkedList<Obstaculo>();
		obstaculos.add(new Obstaculo(entorno.ancho() + 200, entorno.alto() - 80));
		obstaculos.add(new Obstaculo(entorno.ancho() + 500, entorno.alto() - 80));
		obstaculos.add(new Obstaculo(entorno.ancho() + 800, entorno.alto() - 80));
		obstaculos.add(new Obstaculo(entorno.ancho() + 1100, entorno.alto() - 80));

		// Disparos
		fuegos = new LinkedList<Fuego>();

		this.entorno.iniciar();
	}

	public void tick() {
		
		//GameOver
		if (princesa.getVidas() <= 0) {

			entorno.dibujarImagen(gameover, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);

			entorno.cambiarFont("Consolas", 25, Color.white);
			entorno.escribirTexto("Puntuación: " + puntuacion.toString(), entorno.ancho() / 3 + 35, entorno.alto() / 2);
			entorno.escribirTexto("Necesitas " + puntuacionParaBoss.toString() + " puntos para pelear contra el jefe",
					entorno.ancho() / 9 - 17, entorno.alto() / 2 + 50);

			entorno.cambiarFont("Cambria Math", 20, Color.white);
			entorno.escribirTexto("*Cierre la ventana*", entorno.ancho() / 3 + 45, entorno.alto() - 50);
			return;
		}
		if (helicoptero.desaparecio()) {
			entorno.dibujarImagen(gameover, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);

			entorno.cambiarFont("Consolas", 25, Color.white);
			entorno.escribirTexto("¡Felicidades!", entorno.ancho() / 3 + 35, entorno.alto() / 2);
			entorno.escribirTexto("¡Has derrotado al jefe y rescatado a Carlos!", entorno.ancho() / 8,
					entorno.alto() / 2 + 50);

			entorno.cambiarFont("Cambria Math", 20, Color.white);
			entorno.escribirTexto("*Cierre la ventana*", entorno.ancho() / 3 + 45, entorno.alto() - 50);
			return;
		}

		// Background
		entorno.dibujarImagen(fondo, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);
		suelo.dibujar(entorno);
		suelo.mover();
		

		// Puntuacion en Pantalla y vidas
		entorno.cambiarFont("Consolas", 20, Color.white);
		entorno.escribirTexto("Puntuación: " + puntuacion.toString(), 30, 120);
		for (int i = 0; i < princesa.Total_Vidas(); i++) {
			entorno.dibujarImagen(corazon, 50 + i * 100, 50, 0, 0.5);
		}

		// Timer y colisiones
		timer++;

		if (timerInmortal == 200) {
			if (princesa.chocasteConSoldado(soldados) || princesa.chocasteConPinchos(obstaculos)) {
				timerInmortal = 0;
				princesa.restarVida();
			}
			if (princesa.chocasteConHelicoptero(helicoptero)) {
				entorno.dibujarImagen(gameover, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);

				entorno.cambiarFont("Consolas", 25, Color.white);
				entorno.escribirTexto("Has sido derrotado por el jefe", entorno.ancho() / 4 - 15, entorno.alto() / 2 + 20);

				entorno.cambiarFont("Cambria Math", 20, Color.white);
				entorno.escribirTexto("*Cierre la ventana*", entorno.ancho() / 3 + 45, entorno.alto() - 50);
				return;
			}
		}

		if (timerInmortal < 200) {
			timerInmortal++;
		}

		// Princesa
		princesa.dibujar(entorno);

		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) || entorno.estaPresionada('h')) {
			princesa.moverIzquierda();
			princesa.mirarHaciaAtras();
		}

		if (entorno.estaPresionada(entorno.TECLA_DERECHA) || entorno.estaPresionada('l')) {
			princesa.moverDerecha();
		}

		if (entorno.estaPresionada(entorno.TECLA_ARRIBA) || entorno.estaPresionada('k')) {
			princesa.saltar();
		}

		if (princesa.hayLimiteDerecha(entorno)) {
			princesa.moverIzquierdaConRebote();
		}

		if (princesa.hayLimiteIzquierda()) {
			princesa.moverDerechaConRebote();
		}

		princesa.caer();

		// Disparo princesa
		if (!entorno.estaPresionada(entorno.TECLA_ESPACIO) && !entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			princesa.mirarHaciaAdelante();
		}

		if (entorno.sePresiono(entorno.TECLA_ESPACIO)) {
			princesa.imagenDisparando();
			fuegos.add(princesa.dispararFuego());
			Clip disparo = Herramientas.cargarSonido("disparo.wav");
			disparo.start();
		}

		for (Fuego fuego : fuegos) {
			if (fuego != null) {
				fuego.dibujar(entorno);
				fuego.mover();
				if (fuego.sePaso()) {
					fuegos.remove(fuego);
					break;
				}
			}

			if (fuego != null && fuego.pegoConHelicoptero(helicoptero)) {
				fuegos.remove(fuego);
				helicoptero.desaperecerHelicoptero();
				break;
			}
		}

		// Soldados
		for (Soldado soldado : soldados) {
			if (soldado != null && timer > 500) {
				soldado.dibujar(entorno);
				soldado.mover();
			}
		}

		for (Soldado soldado : soldados) {

			if (soldado != null && soldado.sePaso()) {
				soldados.remove(soldado);
				soldados.add(new Soldado(entorno.ancho() + NumeroRandom, entorno.alto() - 115)); 
			}

			for (Fuego fuego : fuegos) {
				if (soldado != null && fuego != null) {
					if (fuego.pegoConSoldado(soldados)) {
						puntuacion += 5;
						fuegos.remove(fuego);
						soldados.remove(soldado);
						soldados.add(new Soldado(entorno.ancho() + NumeroRandom, entorno.alto() - 115)); 
						break;
					}
				}
			}
		}

		// Obstaculos
		for (Obstaculo obstaculo : obstaculos) {
			if (obstaculo != null && timer > 150) {
				obstaculo.dibujar(entorno);
				obstaculo.mover();
			}
		}

		for (Obstaculo obstaculo : obstaculos) {
			if (obstaculo != null && obstaculo.sePaso()) {
				obstaculos.remove(obstaculo);
				obstaculos.add(new Obstaculo(entorno.ancho() + 400, entorno.alto() - 80));
				break;
			}
		}

		// Estrella: puntuacion extra
		estrella.dibujar(entorno);
		estrella.mover();

		if (princesa.agarrasteEstrella(estrella)) {
			estrella.respawnearEstrella(entorno);
			puntuacion += 15;
		}

		if (estrella.sePaso()) {
			estrella.respawnearEstrella(entorno);
		}

		// Helicoptero: Final Boss
		if (puntuacion >= puntuacionParaBoss) {
			for (Soldado soldado : soldados) {
				soldado.desaperecerSoldado();
			}
			for (Obstaculo obstaculo : obstaculos) {
				obstaculo.desaparecer();
			}
			helicoptero.dibujar(entorno);
			helicoptero.mover();
			if (helicoptero.sePaso()) {
				helicoptero = new Helicoptero(entorno.ancho() + 200, entorno.alto() / 2 + 100);
			}
		}

		
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
