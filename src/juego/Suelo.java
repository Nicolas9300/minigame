package juego;

import java.awt.Image;

import entorno.*;

public class Suelo {

	private double x;
	private double y;
	private double velocidad;
	private Image suelo;

	public Suelo(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 2;
		this.suelo = Herramientas.cargarImagen("Suelo.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(suelo, x, y, 0, 1);
		x -= velocidad;
		if (x <= 400 - 64) {
			x = 400;
		}
	}

	// hubiese quedado más prolijo si lo de mover el suelo estaba separado de dibujar
	public void mover() {
		// TODO Auto-generated method stub
	}
}
