package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Soldado {

	private double x;
	private double y;
	private double velocidad;
	private Image soldado;

	public Soldado(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 2;
		this.soldado = Herramientas.cargarImagen("Soldado.gif");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(soldado, x, y, 0, 1);
	}

	public void mover() {
		x -= velocidad;
	}

	public boolean sePaso() {
		return x < 0;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double anchoDelSoldado() {
		return soldado.getWidth(null);
	}

	public double altoDelSolado() {
		return soldado.getHeight(null);
	}
	
	public void desaperecerSoldado() {
		x = 5000;
	}
}
