package juego;

import java.awt.Image;
import java.util.LinkedList;

import entorno.Entorno;
import entorno.Herramientas;

public class Fuego {

	private double x;
	private double y;
	private double velocidad;
	private Image fuego;

	public Fuego(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 5;
		this.fuego = Herramientas.cargarImagen("Fuego.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(fuego, x + 100, y - 30, 0, 1);
	}

	public void mover() {
		x += velocidad;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int anchoDelFuego() {
		return fuego.getWidth(null);
	}

	public int altoDelFuego() {
		return fuego.getHeight(null);
	}

	public boolean sePaso() {
		return x > 850;
	}

	public boolean pegoConHelicoptero(Helicoptero h) {
		return x + anchoDelFuego() / 2 >= h.getX() - h.anchoDelHelicoptero() / 2 && y - altoDelFuego() / 2 >= h.getY() - h.altoDelHelicoptero() / 2
				&& y + altoDelFuego() / 2 <= h.getY() + h.altoDelHelicoptero() / 2;
	}

	public boolean pegoConSoldado(LinkedList<Soldado> s) {
		return x + anchoDelFuego() / 2 >= s.getFirst().getX() - s.getFirst().anchoDelSoldado() / 2
				&& x - anchoDelFuego() / 2 <= s.getFirst().getX() + s.getFirst().anchoDelSoldado() / 2
				&& y + altoDelFuego() / 2 >= s.getFirst().getY() - s.getFirst().altoDelSolado() / 2;
	}
}
